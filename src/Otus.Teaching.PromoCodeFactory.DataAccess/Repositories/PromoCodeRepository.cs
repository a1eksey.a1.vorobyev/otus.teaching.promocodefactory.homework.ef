﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromoCodeRepository : EfRepository<PromoCode>, IPromoCodeRepository
    {
        public PromoCodeRepository(DataContext context) : base(context)
        {
        }
    }
}
