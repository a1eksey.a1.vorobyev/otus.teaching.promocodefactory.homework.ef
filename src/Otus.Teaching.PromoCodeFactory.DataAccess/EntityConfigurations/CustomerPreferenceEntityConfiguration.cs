﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations
{
    public class CustomerPreferenceEntityConfiguration : IEntityTypeConfiguration<CustomerPreference>
    {
        public void Configure(EntityTypeBuilder<CustomerPreference> builder)
        {
            builder.ToTable("CustomerPreference");

            builder.HasKey(c => new { c.PreferenceId, c.CustomerId });

            builder
                .HasOne(c => c.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(c => c.CustomerId);

            builder
                .HasOne(c => c.Preference)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(c => c.PreferenceId);
        }
    }
}
