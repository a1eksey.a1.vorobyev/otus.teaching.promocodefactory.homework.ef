﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        // [MaxLength(150)] 
        public string Name { get; set; }

        // [MaxLength(250)]
        public string Description { get; set; }
        public ICollection<Employee> Employees { get; set; }
    }
}